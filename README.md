# Jose Gonzalez react code challenge

## El siguiente código está desarrollado bajo una plantilla creada bajo un template react js CRA que suelo usar para realizar pruebas.

## Estructura

### SignIn: \src\@jumbo\components\Common\authComponents\SignIn.js
* En este archivo se desarrolla el formulario Jsx para el login.


### Login: \src\services\auth\jwt\index.js
* En este archivo se desarrolla la lógica inicial del login, para lo cual se hizo uso de la tecnología Redux y el localStorage.
* Metodos utilizados: onLogin, onLogout, getAuthUser.


### Lógica: \src\routes\Pages\ChallengeCode
* En esta carpeta se encuentran alojados los distintos archivos y carpetas en relación al reto de código.

### Servicios: \src\services\database\members\index.js
* En este archivo encontraremos el objeto "members" el cual contiene los metodos "getMembers" y "saveDataMember" los cuales hacen los respectivos llamados a la Api.



## Instrucciones
* Clonar este repositorio.
* Instalar las dependencias: npm install.
* Levantar el proyecto: npm run start
* Iniciará en http://localhost:3000/
