import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { AuhMethods } from '../../../services/auth';
import { CurrentAuthMethod } from '../../../@jumbo/constants/AppConstants';
import GridContainer from '../../../@jumbo/components/GridContainer';
import PageContainer from '../../../@jumbo/components/PageComponents/layouts/PageContainer';
import Box from '@material-ui/core/Box';
import IntlMessages from '../../../@jumbo/utils/IntlMessages';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import TextField from '@material-ui/core/TextField';
import { useIdleTimer } from 'react-idle-timer';

// Custom components.
import TableData from './customComponents/TableComponent/index';

// Validators
import { validateSsn, validateForm } from './validators';

// Services.
import { members } from '../../../../src/services/database/members';

const breadcrumbs = [
  { label: 'Home', link: '/' },
  { label: 'Challenge code', isActive: true },
];

const initialState = {
  firstName: '',
  lastName: '',
  address: '',
  ssn: ''
};

const ChallengeCode = () => {

  const dispatch = useDispatch();
  const [formData, setFormData] = useState({ ...initialState });
  const [membersData, setMembers] = useState([]);
  const [ssnError, setSsnError] = useState('');
  const [validData, setValidData] = useState(true);

  useEffect(() => { 

    async function getMembers() {
      getDbMembers();
    }

    getMembers();
  }, []);

  // Actualize the table of data members after 2 minutes of user inactivity.
  const handleOnIdle = event => {

    getDbMembers();
  }

  const handleOnActive = event => {}

  const handleOnAction = event => {}

  const { getRemainingTime, getLastActiveTime } = useIdleTimer({
    timeout: 2 * 60 * 1000,
    onIdle: handleOnIdle,
    onActive: handleOnActive,
    onAction: handleOnAction,
    debounce: 500
  })

  const getDbMembers = async () => {
    const data = await members.getMembers();

    // If status of the called api services return status 401, token has expired,  the function logout will be called.
    if (data.response && data.response.status == '401') {
      dispatch(AuhMethods[CurrentAuthMethod].onLogout());
    } else {
      setMembers(data);
    }

  }

  const setStateFormData = e => {

    const { name, value } = e.target;

    if (name == 'ssn') {
      setSsnError('');
      
      const validateFormatSsn = validateSsn(value);

      if(!validateFormatSsn){
        setSsnError('Invalid SSN');
        
      }
     
    }

    setFormData(prevState => ({
      ...prevState,
      [name]: value
    }));

    const {firstName, lastName, address} = formData;
    const result = validateForm(value, firstName, lastName, address);

    setValidData(result);
    
  };

  const onSubmit = async () => {
    setSsnError('');
    const response = await members.saveDataMember(formData);

    if (response.response && response.response.data.code == 'BadRequest') {
      setSsnError(response.response.data.message);
    } else {

      // It will actualize members data without call getDbMembers method.
      setMembers(prevArray => [...prevArray, response])
    }

  };

  const onReset = () => {
    setFormData({ ...initialState });
  }

  return (
    <>
      <PageContainer heading={<IntlMessages id="pages.challengeCode" />} breadcrumbs={breadcrumbs}>
        <GridContainer>
          <Grid item xs={12}>
            <Box>
              <IntlMessages id="pages.challengeCode" />
            </Box>
          </Grid>
          <Grid item xs={6}>
            <Box>
              <form>
                <Box mb={2}>
                  <TextField
                    label={<IntlMessages id="pages.firstName" />}
                    fullWidth
                    onChange={e => setStateFormData(e)}
                    name="firstName"
                    value={formData.firstName}
                    margin="normal"
                    variant="outlined"
                  />
                </Box>
                <Box mb={2}>
                  <TextField
                    label={<IntlMessages id="pages.lastName" />}
                    fullWidth
                    onChange={e => setStateFormData(e)}
                    name="lastName"
                    value={formData.lastName}
                    margin="normal"
                    variant="outlined"
                  />
                </Box>

                <Box mb={2}>
                  <TextField
                    label={<IntlMessages id="pages.address" />}
                    fullWidth
                    onChange={e => setStateFormData(e)}
                    name="address"
                    value={formData.address}
                    margin="normal"
                    variant="outlined"
                  />
                </Box>

                <Box mb={2}>
                  <TextField
                    label={<IntlMessages id="pages.ssn" />}
                    fullWidth
                    onChange={e => setStateFormData(e)}
                    name="ssn"
                    value={formData.ssn}
                    margin="normal"
                    variant="outlined"
                    error={ssnError != ''}
                    helperText={ssnError != '' ? ssnError : ' '}
                    
                  />
                </Box>

                <Box display="flex" alignItems="center" justifyContent="space-between" mb={5}>
                  <Button onClick={onSubmit} variant="contained" color="primary" disabled={validData}>
                    <IntlMessages id="pages.send" />
                  </Button>
                  <Button onClick={onReset} variant="contained" color="primary">
                    <IntlMessages id="pages.reset" />
                  </Button>
                </Box>
              </form>
            </Box>

          </Grid>

          {/* Component table for show data members */}
          <TableData membersData={membersData} />

        </GridContainer>
      </PageContainer>
    </>
  );
};

export default ChallengeCode;
