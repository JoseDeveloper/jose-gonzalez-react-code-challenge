import React from 'react';
import Grid from '@material-ui/core/Grid';
import { makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";

const useStyles = makeStyles({
  table: {
    minWidth: 350,
    maxWidth: 650
  }
});

export default function TableComponent({ membersData }) {

  const classes = useStyles();
  
    return (
      <>
            <Grid item xs={6}>
              <TableContainer component={Paper}>
                <Table className={classes.table} aria-label="simple table">
                  <TableHead>
                    <TableRow>
                      <TableCell>First name</TableCell>
                      <TableCell align="right">Last name</TableCell>
                      <TableCell align="right">Address</TableCell>
                      <TableCell align="right">Ssn</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {membersData && membersData.map((row, i) => (
                      <TableRow key={i}>
                        <TableCell component="th" scope="row">
                          {row.firstName}
                        </TableCell>
                        <TableCell align="right">{row.lastName}</TableCell>
                        <TableCell align="right">{row.address}</TableCell>
                        <TableCell align="right">{row.ssn}</TableCell>
                      </TableRow>
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </Grid>
      </>
    );
  };
  