import { fetchError, fetchStart, fetchSuccess } from '../../../redux/actions';
import { setAuthUser, updateLoadUser } from '../../../redux/actions/Auth';
import React from 'react';
import axios from './config';

const JWTAuth = {
  onRegister: ({ name, email, password }) => {
    return dispatch => {
      dispatch(fetchStart());
      axios
        .post('auth/register', {
          email: email,
          password: password,
          name: name,
        })
        .then(({ data }) => {
          if (data.result) {
            localStorage.setItem('token', data.token.access_token);
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.token.access_token;
            dispatch(fetchSuccess());
            dispatch(JWTAuth.getAuthUser(true, data.token.access_token));
          } else {
            dispatch(fetchError(data.error));
          }
        })
        .catch(function(error) {
          dispatch(fetchError(error.message));
        });
    };
  },

  // Challenge code.
  onLogin: ({ user, password }) => {
    return dispatch => {
      try {
        dispatch(fetchStart());
        axios
          .post('auth', {
            username: user,
            password: password,
          })
          .then(({ data }) => {  
            if (data) {

              localStorage.setItem('token', data.token);
              localStorage.setItem('authUser', data);
              axios.defaults.headers.common['Authorization'] = 'Bearer ' + data.token;
              dispatch(fetchSuccess());
              dispatch(setAuthUser(data));
            } else {
              dispatch(fetchError(data.error));
            }
          })
          .catch(function(error) {
            dispatch(fetchError(error.message));
          });
      } catch (error) {
        dispatch(fetchError(error.message));
      }
    };
  },

  // Challenge code.
  onLogout: () => {
    return dispatch => {
      dispatch(fetchStart());
      localStorage.removeItem('token');
      dispatch(setAuthUser(null));
    };
  },

  // Challenge code.
  getAuthUser: (loaded = false, token) => { 
    return dispatch => {
      dispatch(fetchStart());
      dispatch(updateLoadUser(loaded));
      const data = localStorage.getItem('authUser');
      dispatch(fetchSuccess());
      dispatch(setAuthUser(data));
    };
  },

  
  onForgotPassword: () => {
    return dispatch => {};
  },
  getSocialMediaIcons: () => {
    return <React.Fragment> </React.Fragment>;
  },
};

export default JWTAuth;
