import axios from 'axios';

export default axios.create({
  //baseURL: `http://127.0.0.1:8000/api/`, 
  baseURL: `http://localhost:8081/`,
  headers: {
    'Content-Type': 'application/json',
  },
});
