import axios from '../../config';

export const members = {
  getMembers: () => {

    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.get('members', {})
      .then(({ data }) => { 
        if (data) {
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });
    return response;
  },

  saveDataMember: (formData) => {
    const { firstName, lastName, address, ssn } = formData;
    const objectToSave = { firstName, lastName, address, ssn };
    
    const token = localStorage.getItem('token');
    axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
    const response = axios.post('members', objectToSave)
      .then(({ data }) => { 
        if (data) {
          return data;
        } else {
          return data.error;
        }
      })
      .catch(function (error) {
        return error;
      });

    return response;
  }

};
